using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("     -------------------");
            Console.WriteLine("     |    Reniel's     |");
            Console.WriteLine("     |   Calculator!   |");
            Console.WriteLine("     |                 |");
            Console.WriteLine("     |                 |");
            Console.WriteLine("     |  + (add)        |");
            Console.WriteLine("     |  - (subtract)   |");
            Console.WriteLine("     |  * (multiply)   |");
            Console.WriteLine("     |  / (divide)     |");
            Console.WriteLine("     |                 |");
            Console.WriteLine("     -------------------");
            Console.Write("Enter the first number: ");
            double num1 = Convert.ToDouble(Console.ReadLine());
            Console.Write("Enter operation: ");
            string op = Console.ReadLine();
            Console.Write("Enter the second number: ");
            double num2 = Convert.ToDouble(Console.ReadLine());
            if (op == "+")
            {
                Console.WriteLine("The sum is " + (num1 + num2));
            }
            else if (op == "-")
            {
                Console.WriteLine("The difference is " + (num1 - num2));
            }
            else if (op == "*")
            {
                Console.WriteLine("The product is " + (num1 * num2));
            }
            else if (op == "/")
            {
                Console.WriteLine("The quotient is " + (num1 / num2));
            }
            else
            {
                Console.WriteLine("Invalid Operator");
            }
            Console.ReadLine();
        }
    }
}